package sber.homework.dz6;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import sber.homework.dz6.dao.UserDao;

@SpringBootApplication
public class Dz6Application implements CommandLineRunner {
	private final UserDao userDao;

	public Dz6Application(UserDao userDao) {
		this.userDao = userDao;
	}

	public static void main(String[] args) {
		SpringApplication.run(Dz6Application.class, args);
	}

	@Override
	public void run(String... arg) throws Exception {

		System.out.println(userDao.findByEmail("asok@mail.com"));
		//Доп задания:
		int x = 7;
		System.out.println(isArmstrongNumber.isArmstrong(x));
		System.out.println(isNaturalNumber.isNatural(x));
	}

}
