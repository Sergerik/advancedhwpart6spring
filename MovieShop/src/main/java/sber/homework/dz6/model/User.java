package sber.homework.dz6.model;

import lombok.*;

import javax.xml.crypto.Data;
import java.time.Month;
import java.time.Year;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class User {
	private int id;
	private String Surname;
	private String Name;
	private Date birthDate;
	private Long phone;
	private String email;
	private String getBooks;
}
