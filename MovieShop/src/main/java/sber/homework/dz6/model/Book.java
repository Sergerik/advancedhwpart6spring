package sber.homework.dz6.model;

import lombok.*;

import javax.xml.crypto.Data;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Book {
private int id;
private String Title;
private String Author;

}
