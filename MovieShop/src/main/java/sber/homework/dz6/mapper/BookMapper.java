package sber.homework.dz6.mapper;

import sber.homework.dz6.model.Book;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Set;

public class BookMapper {
	public Book bookMapper(ResultSet resultSet) throws SQLException {
		Book book = new Book();
		book.setId(resultSet.getInt("id"));
		book.setTitle(resultSet.getString("title"));
		book.setAuthor(resultSet.getString("author"));
		return book;
	}
}
