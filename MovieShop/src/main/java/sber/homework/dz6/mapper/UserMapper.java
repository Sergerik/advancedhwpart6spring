package sber.homework.dz6.mapper;

import sber.homework.dz6.model.User;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserMapper {
	public User userMapper(ResultSet resultSet) throws SQLException {
		User user = new User();
		user.setId(resultSet.getInt("id"));
		user.setName(resultSet.getString("name"));
		user.setSurname(resultSet.getString("surname"));
		user.setPhone(resultSet.getLong("phone"));
		user.setEmail(resultSet.getString("email"));
		user.setBirthDate(resultSet.getDate("birth_date"));
		user.setGetBooks(resultSet.getString("get_books"));
		return user;
	}
}
