package sber.homework.dz6;

import org.springframework.context.annotation.Bean;

import java.util.ArrayList;
import java.util.List;

public class isArmstrongNumber {
	@Bean
	public static boolean isArmstrong(int number) {
		List<Integer> list = new ArrayList<>();
		int x=number;
		while (x != 0) {
			list.add(x % 10);
			x = x / 10;
		}
		int y = 0;
		for (int i = 0; i < list.size(); i++) {
			y = y + ((int) Math.pow(list.get(i), list.size()));
		}
		return number == y;
	}

}

