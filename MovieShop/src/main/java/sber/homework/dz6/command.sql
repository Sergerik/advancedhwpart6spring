create table users
(
    id         serial primary key,
    surname    varchar(50)   not null,
    name       varchar(50)   not null,
    birth_date date          not null,
    phone      bigint        unique not null,
    email      varchar(50)   unique not null,
    get_books  varchar(5000) not null

);
create table books
(
    id    serial primary key,
    title varchar(100) unique not null,
    author varchar(150) not null
);

insert into books (title, author)  values ('Война и Мир','Л. Н. Толстой');
insert into books (title, author)  values ('Тихий Дон','Михаил Шолохов');
insert into books (title, author)  values ('Гарри Поттер','Джоан Роулинг');
insert into books (title, author)  values ('Илиада','Гомер');
insert into books (title, author)  values ('Цветик-семицветик','Валкнтин Катаев');
insert into books (title, author)  values ('Доктор Живаго','Борис Пастернак');
insert into books (title, author)  values ('Волк и семеро козлят','Братья Гримм');

drop table books