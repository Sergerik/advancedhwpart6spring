package sber.homework.dz6.dao;

import org.springframework.stereotype.Component;
import sber.homework.dz6.mapper.BookMapper;
import sber.homework.dz6.model.Book;

import java.sql.*;

public class BookDao {
	private BookMapper bookMapper = new BookMapper();
	private final Connection connection;
	public BookDao(Connection connection) {
		this.connection=connection;
	}

	public Book findByTitle(String title) throws SQLException {
		PreparedStatement preparedStatement = connection.prepareStatement("select * from books where title=?");
		preparedStatement.setString(1, title);
		ResultSet resultSet = preparedStatement.executeQuery();
		Book book = new Book();
		while (resultSet.next()) {
			book = bookMapper.bookMapper(resultSet);
		}
		return book;

	}
}
