package sber.homework.dz6.dao;

import sber.homework.dz6.model.Book;

import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class UserDao {
	private final Connection connection;
	private final BookDao bookDao;

	public UserDao(Connection connection, BookDao bookDao) throws SQLException {
		this.connection = connection;
		this.bookDao = bookDao;
	}

	public void createUser(String name, String surname, Date birthDate, Long phone, String email, String getBooks) throws SQLException {
		PreparedStatement preparedStatement = connection.prepareStatement("insert into users(name, surname, birth_date, phone, email, get_books) values (?,?,?,?,?,?)");
		preparedStatement.setString(1, name);
		preparedStatement.setString(2, surname);
		preparedStatement.setDate(3, birthDate);
		preparedStatement.setLong(4, phone);
		preparedStatement.setString(5, email);
		preparedStatement.setString(6, getBooks);
		preparedStatement.executeUpdate();
	}

	public List<Book> findByEmail(String str) throws SQLException {
		PreparedStatement preparedStatement = connection.prepareStatement("select get_books from users where email=?");
		preparedStatement.setString(1, str);
		ResultSet resultSet = preparedStatement.executeQuery();
		String allBooks = "";
		while (resultSet.next()) {
			allBooks = resultSet.getString("get_books");
		}
		System.out.println(allBooks);
		List<String> list = new ArrayList<>();
		int x = 0;
		for (int i = 0; i < allBooks.length(); i++) {
			if (allBooks.charAt(i) == ',')
				{
					list.add(allBooks.substring(x, i));
					x = i + 1;
				}
				if ((i - x) == allBooks.length() - x - 1) {
					list.add(allBooks.substring(x));
				}
			}
			List<Book> books = new LinkedList<>();
			for (int i = 0; i < list.size(); i++) {
				books.add(bookDao.findByTitle(list.get(i)));
			}
			return books;
		}

	}

