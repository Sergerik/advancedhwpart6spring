package sber.homework.dz6;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import sber.homework.dz6.dao.BookDao;
import sber.homework.dz6.dao.UserDao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@Configuration
public class DBConfig {


	@Bean
	@Scope("singleton")
	public Connection connection() throws SQLException {
		return DriverManager.getConnection(
				"jdbc:postgresql://localhost:5434/dz6",
				"postgres",
				"12345"
		);
	}
	@Bean
	@Scope("prototype")
	public UserDao userDao() throws SQLException  {
		return new UserDao(connection(), bookDao());
	}
	@Bean
	@Scope("prototype")
	public BookDao bookDao() throws SQLException  {
		return new BookDao(connection());
	}

}
