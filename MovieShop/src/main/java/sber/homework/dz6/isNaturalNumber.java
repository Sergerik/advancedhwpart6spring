package sber.homework.dz6;

import org.springframework.context.annotation.Bean;

public class isNaturalNumber {
	@Bean
	public static boolean isNatural(int x) {
		if (x < 1) return false;
		if (x == 2) return true;
		if (x % 2 == 0) return false;
		else {
			for (int i = 3; i * i < x; i += 2) {
				if (x % i == 0) return false;
			}
		}
		return true;
	}
}
